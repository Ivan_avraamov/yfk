<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" rel="stylesheet" />        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
	<?php remove_action('wp_head', 'wp_generator'); ?>
	<?php remove_action('wp_head', 'rsd_link'); ?>
  <?php remove_action('wp_head', 'wlwmanifest_link'); ?>
  <?php remove_filter('the_content', 'wpautop'); ?>
	<?php wp_head(); ?>
    <!-- Подключение библиотеки карты -->
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU" type="text/javascript"></script>
</head>
<body <?php body_class(); ?>>
<div id="wrap">
  <div id="header">
    <?php if (is_front_page()) { ?> 
    <span id="logo"></span>
    <?php } else { ?>
    <a href="<?php bloginfo('home'); ?>" id="logo">ЮФК</a>
    <?php } ?>
    <p id="slogan">юридическая фирма</p>
    <!--<div id="lang">
      <a href="#">укр</a>
      <a href="#">Eng</a>
    </div>-->
    <form id="search" method="GET" action="<?php bloginfo('home'); ?>">
    <div>
      <input type="text" onblur="this.value=(this.value=='') ? 'Поиск' : this.value;" onfocus="this.value=(this.value=='Поиск') ? '' : this.value;" value="Поиск" name="s" id="s" />
      <input id="search_button" type="submit" value="" />
    </div>    
    </form>
  </div>
  <ul id="nav">
    <?php wp_list_pages("title_li=&include=2"); ?>
    <?php wp_list_pages("sort_column=menu_order&title_li=&exclude=2,12,14"); ?>
    <?php wp_list_categories("title_li="); ?>
    <?php wp_list_pages("title_li=&include=12,14"); ?>
  </ul>
  <div id="content_wrap">
  <div id="content">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
      <?php the_content(__('(more...)')); ?>
      <?php wp_link_pages(); ?>
      <?php edit_post_link(__('Edit This')); ?>
    <?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>
  </div>
  </div>
  <div id="footer">
    <div id="copyright">
      <p>© 2005—<?php echo date("Y"); ?> «ЮФК»</p>
      <a href="http://tadatuta.ru/">Разработка сайта</a> — <a href="http://tadatuta.ru/">студия Тадатута</a>
    </div>
    <div id="contacts">
      <p>98600, АРК, г. Ялта <br />
      ул. К. Маркса, 12, кв. 7<br />
      раб. тел./факс: (0654) 32-04-65<br />
      e-mail: <a href="mailto:office@yufk.com.ua">office@yufk.com.ua</a><br />
      а/я № 469</p>
    </div>
    </div>
</div>
	<?php wp_head(); ?>
</body>
</html> 
