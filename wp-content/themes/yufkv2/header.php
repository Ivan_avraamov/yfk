<!DOCTYPE HTML>
<html>
	<head>
		<title><?php wp_title(); ?></title>
		<meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
        <link rel="icon" href="<?php bloginfo('template_url'); ?>/img/icon.ico">
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
	</head>
            <body>
            <div id="wrapper">
                <div class="header">
                    <div class="header-line"></div>
                    <?php if ( is_front_page() ) { ?>
                    <ul class="menu-nav">
                        <li class="page-item"><span class="curent-main">Главная</span></li>
                    <?php } else{ ?>
                        <a href="<?php echo site_url(); ?>" class="logo-min"></a>
                        <ul class="menu-nav inside-nav">
                            <?php wp_list_pages("title_li=&include=392");?>
                            <?php } ?>
                            <?php
                            wp_list_categories("title_li=&include=");

                            wp_list_pages("sort_column=menu_order&title_li=&exclude=2,12,14,392");
                            wp_list_pages("title_li=&include=2,12,14");
                            ?>
                            <ul>
                                <?php
                    global $post;
                        $args = array( 'numberposts' => -1, 'category' => 3 );
                        $myposts = get_posts( $args );
                    foreach( $myposts as $post ) :  setup_postdata($post); ?>
                         <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endforeach; ?>
                            </ul>

                        </ul>
                        <?php if ( is_front_page() ) { ?>
                            <div class="logo"> </div>
                            <h1 class="header_slogan_text">Эффективные решения!</h1>
                        <?php } ?>
                </div>
