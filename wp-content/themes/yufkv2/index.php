<?php get_header(); ?>
            <div class="content">
                <?php if (is_front_page()){ ?>
                    <?php include(TEMPLATEPATH . '/home.php'); ?>
                <?php } else{ ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php if ( in_category('1') || in_category('3') ) { ?>
                            <div class="post-item">
                                <h2>
                                    <?php if (!is_single()) { ?>
                                        <a href="<?php the_permalink() ?>">
                                    <?php } ?>
                                    <?php the_title(); ?>
                                    <?php if (!is_single()) { ?></a><?php } ?>
                                </h2>
                        <?php } else{ ?>
                            <h2><?php the_title(); ?></h2>

                        <?php } ?>
                                <?php
                                the_content(__('(more...)'));
                                wp_link_pages();
                                edit_post_link(__('Edit This'));
                                ?>
                                <?php if ( in_category('1')|| in_category('3')) { ?>
                                   </div>
                                <?php }

                                 ?>
                    <?php endwhile; else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                    <?php posts_nav_link(' &#8212; ', __('&laquo; Более новые '), __('Более старые &raquo;')); ?>
                <?php } ?>
            </div>
<?php get_footer(); ?>
