    <div class="left-side">
            <h2 class="content-title-tag">Мы предоставляем юридические услуги в Крыму</h2>
            <ul class="content-menu">
                <?php
                    global $post;
                        $args = array( 'numberposts' => -1, 'category' => 3 );
                        $myposts = get_posts( $args );
                    foreach( $myposts as $post ) :  setup_postdata($post); ?>
                         <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endforeach; ?>
            </ul>
        </div>
        <div class="right-side">
            <div class="contacts">
                <img src="<?php echo site_url(); ?>/wp-content/uploads/2013/07/hammer.jpg" alt="hammer" class="contact-img"/><p>Если вам срочно необходима
                    юридическая помощь или консультация, звоните нам по телефону: <span class="phone-num">+38 (0654) 32-04-65</span> или пишите на эл. почту:
                    <a href="mailto:office@yufk.com.ua">office@yufk.com.ua</a></p>
            </div>
            <div class="company-news">
                <h2 class="content-title-tag">Новости компании</h2>
                <ul class="news-list">
                    <?php if(have_posts()) : ?>
                        <?php query_posts('cat=1&posts_per_page=3'); ?>
                        <?php while(have_posts()) : the_post(); ?>
                            <li id="post-<?php the_ID(); ?>">
                                <p class="news-data"><?php the_time('j F'); ?></p>
                                <p class="news-content">
                                «<a href="<?php the_permalink() ?>"><?php the_post_thumbnail(); ?><?php the_title(); ?></a>»...</p>
                            </li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
                <div class="more-news"><span>Больше новостей </span><a href="<?php echo site_url(); ?>/?cat=1">в архиве</a></div>
            </div>
        </div>
